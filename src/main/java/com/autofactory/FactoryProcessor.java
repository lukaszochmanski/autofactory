package com.autofactory;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@AutoService(Processor.class)
public class FactoryProcessor extends AbstractProcessor {

    private Filer filer;
    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (areAllElementsAreOfKindClass(set, roundEnvironment)) {
            generateFiles(set, roundEnvironment);
        }
        return false;
    }

    private void generateFiles(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        try {
            FactoryBuilder.INSTANCE.generate(filer, getAnnotatedElementsOfKindClass(set, roundEnvironment));
        } catch (IOException e) {
            error(e.getMessage());
        }
    }

    private boolean areAllElementsAreOfKindClass(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        List<? extends Element> annotatedElementsOfNotKindClass = getAnnotatedElementsOfNotKindClass(set, roundEnvironment);
        annotatedElementsOfNotKindClass.stream()
                .map(e -> "Class can be annotated with " + e.getSimpleName())
                .forEach(this::error);
        return annotatedElementsOfNotKindClass.isEmpty();
    }

    private List<? extends Element> getAnnotatedElementsOfKindClass(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        return set.stream()
                .map(roundEnvironment::getElementsAnnotatedWith)
                .flatMap(Collection::stream)
                .filter(this::isKindClass)
                .collect(Collectors.toList());
    }

    private List<? extends Element> getAnnotatedElementsOfNotKindClass(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        return set.stream()
                .map(roundEnvironment::getElementsAnnotatedWith)
                .flatMap(Collection::stream)
                .filter(not(this::isKindClass))
                .collect(Collectors.toList());
    }

    public static <T> Predicate<T> not(Predicate<T> t) {
        return t.negate();
    }

    private boolean isKindClass(Element el) {
        return ElementKind.CLASS == el.getKind();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new LinkedHashSet<>();
        annotations.add(Factory.class.getCanonicalName());
        annotations.add(javax.persistence.Entity.class.getCanonicalName());
        annotations.add(javax.persistence.Embeddable.class.getCanonicalName());
        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    private void error(String message) {
        messager.printMessage(Diagnostic.Kind.ERROR, message);
    }
}
